# Dicionário de Google Ads para Afiliados

<p align="center">🚀 <i>É muito importante compreender os principais termos tal como toda linguagem que será utilizada ao longo de sua jornada como afiliado. Algo que vale não somente para o Google Ads, mas para sua carreira no Marketing Digital.</i></p>
<p align="center"><i>Para melhor absorção do conteúdo, sugiro que estude e se familiarize com esse dicionário. Como dito, o conhecimento e a estratégia são as grandes chaves do sucesso em todo negócio.</i></p><br>

- `Ticket` - É o valor do produto que você vai vender.
- `Graus e BARS do Produto` - É a intensidade ou mesmo a referência do quanto o produto está vendendo nas plataformas. Quanto maior o grau/bar, mais ele está sendo vendido. São termos típicos de cada plataforma. Servem como um “termômetro” do comportamento das vendas daquele produto.
- `Conversão` - É uma conquista do objetivo da campanha. É quando, por exemplo, você realiza uma venda em uma campanha de venda. Poderia ser uma inscrição na página, caso a sua campanha fosse voltada para a captura de leads. No nosso caso principal, cada venda realizada é uma conversão. Podemos considerar então, que para uma campanha de venda:
  Conversão = Venda
- `Leads` - Lead é um termo inglês que significa conduzir, no marketing os leads são o público interessado. Assim, quando se fala em atrair ou capturar leads, estamos falando de estratégias para guardar os contatos desse público-alvo como o objetivo de interações futuras, seja para tratar como uma referência de interesses ou comunicação direta.
- `Campanha` - No Google Ads é a ação estratégica onde você configura o plano de conversão e a publicação do seu anúncio.
- `Pixel` - É onde são armazenados os dados dos compradores. Ele é essencial para uma campanha evoluir. Ele que vai identificar os padrões de compradores do nosso produto armazenando seus dados dentro da base de dados do sistema. Através desses padrões, ele colocará seu anúncio somente em leilões onde ele identificar perfil correspondente aos os dados armazenados.
- `Pixel em Nível de Campanha` - É quando você configura o pixel para marcar as conversões em nível da campanha. Ou seja, quero que todos esses dados armazenados se referenciem apenas à minha determinada campanha.
- `Pixel em Nível de Conta` - É quando você configura o pixel para marcar as conversões em nível da conta. Ou seja, quero que todos esses dados armazenados se referenciem a toda minha conta (todas as campanhas nela contidas) e não apenas a uma campanha específica.
- `Otimizar` - São processos que vão fazendo que aquela situação melhore ao decorrer dos dias. São ajustes para deixar uma campanha sempre melhor, otimizada.
- `MCC - Minha Conta Cliente` - Nada mais é que uma conta que administra todas outras contas vinculadas a ela. Uma conta mãe, que toma conta de todas outras contas que você vincular a ela.
- `Dor` - É a necessidade do cliente. Todo produto é criado para resolver um problema específico, então, chamamos esse problema de DOR.
- `URL Final` - É o site onde o cliente vai cair ao clicar.
- `Domínio` - É o “nome” do site. Ex: www.caiocalderaro.com
- `Campanha Inteligente` - É uma campanha com muitos dados de conversões, ou seja, uma campanha com muitos dados de vendas.
- `Modelo de Acompanhamento` - Opção onde coloca seu link de afiliado para ganhar suas comissões.
- `Cookie` - É a captura de dados da máquina, em especial o IP, que é uma forma de CPF do dispositivo. Assim, o cookie faz a marcação do seu link de afiliado quando um cliente acessa a página de um produto que você está divulgando, guardando o dado do comprador e referenciando o código do afiliado na compra. Quando um cliente clica nessa página, ele será “cookado” pelo seu link de afiliado.
- `Correspondência de Palavra-Chave` - São as variações que você usa nas palavras-chaves para dar mais liberdade para o google mostrar seus anúncios ou não.
- `Leilão` - É o leilão que o Google faz para mostrar o seu anúncio. Você paga “x” na concorrência do leilão para seu anúncio aparecer.
- `Escala` - Processo de projeção / aumento das vendas.
- `CPC` - Custo por clique.
- `Impressão` - Número de exibições do seu anúncio.
- `CTR` - É a taxa de interação. A relação entre o número de pessoas que clicaram no anúncio dividido pelo número de pessoas que viram o anúncio (impressões).
  Ex: 100 impressões e 10 cliques
  CTR de 10%
- `Nicho` - É o segmento que você escolhe atuar.
  Ex: Nutrição.
  É um nicho de atuação.
- `Nicho Black` - É um nicho de sexualidade (adulto) ou de promessas fortes (que não se pode garantir), é um segmento “proibido” pelas políticas do Google, porém, existem formas de conseguir anunciar para esse nicho.
- `Pressel` - Usadas como recurso em nicho black (obrigatório), é uma página de direcionamento (pré-venda), que conduz a pessoa para um outro site.
  É uma página de “confirmação” para a pessoa acessar o outro site.
- `Advertorial` - É um modelo de artigo, como uma notícia, normalmente falando sobre o assunto e apresentando o produto como solução. Uma estratégia de persuasão utilizada em anúncios para a dor. Traz autoridade e esclarece o produto para o consumidor, além de um impacto maior na persuasão. É uma história com as dores que o produto resolve, com grande força para levar o cliente a uma conversão.
- `Custo por Conversão` - É o quanto foi gasto para realizar uma venda.
- `Hospedagem` - Onde se hospedar um site. É preciso hospedar o domínio em algum local para ter acesso ao mesmo. Ex de hospedagem: Hostgator
- `Wordpress` - É uma ferramenta de edição / configuração para você montar o seu site e suas páginas que normalmente está vinculada a hospedagem.
- `Iframe` - É um código utilizado como estratégia sendo aplicado em páginas para você marcar o seu cookie de afiliado. Normalmente usado em pre-sell. Mas deve ser utilizado com cautela, pois pode ser identificado como software malicioso, ainda que não seja.
- `Anunciar para Dor` - Quando você faz um anúncio para o problema que seu cliente busca solucionar.
- `Anunciar para Marca` - Quando você anuncia para o nome do produto.
- `Funil` - É uma espécie de segmentação dos interesses da pesquisa do cliente. Referência a amplitude de um determinado assunto ou produto.
- `Topo do Funil` - Onde tem maior volume. É mais amplo e ao mesmo tempo têm maior escala.
- `Meio do Funil` - Volume médio e se enquadra no processo de decisão.
- `Fundo do Funil` - Quando está bem fino (específico). Apresenta menor volume e buscas mais qualificadas.
- `Remarketing` - É quando alguém já teve algum tipo de interação na sua página. No caso, você faz um anúncio exatamente para essa pessoa que já te conheceu através de algum conteúdo ou visitas no seu site. É um cliente em potencial. Já houve um certo interesse no seu produto.
